<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<!-- <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentyseventeen' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentyseventeen' ), 'WordPress' ); ?></a> -->

	<?php 
	if ( has_nav_menu( 'about' ) ) {
		wp_nav_menu( array(
			'theme_location' => 'about',
			'menu_id'        => 'about-menu',
		) );
	}
	?>


</div><!-- .site-info -->
