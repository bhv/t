<?php
/**
 * Template for displaying search forms in Twenty Seventeen
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 * 
 * changed 
 * name -  s   to   q
 * action  - default one is  - "<?php echo esc_url( home_url( '/	' ) ); ?>"
 * 
 * #updates  - may need to change 'action' url  -  $search_action
 */

?>

<?php 
$unique_id = esc_attr( uniqid( 'search-form-' ) );

$search_action = 'https://www.holithemes.com/search';
// $search_action = home_url( '/search' );

?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( $search_action ); ?>">
	<label for="<?php echo $unique_id; ?>">
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'twentyseventeen' ); ?></span>
	</label>
	<input type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'twentyseventeen' ); ?>" value="<?php echo get_search_query(); ?>" name="q" />
	<button type="submit" class="search-submit"><?php echo twentyseventeen_get_svg( array( 'icon' => 'search' ) ); ?><span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'twentyseventeen' ); ?></span></button>
</form>
